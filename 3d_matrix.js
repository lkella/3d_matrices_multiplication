/**
 * checks the validation of given matrix
 * @param {*} matrix 
 * @returns boolean
 */
function validate_matrix(matrix) {
	matrix_rows = matrix.length;
	matrix_cols = matrix[0].length;
	for (var i = 0; i < matrix_rows; i++) {
		if (matrix[i].length != matrix[i + 1].length) {
			return false;
		} else {
			for (var j = 0; j < matrix_rows; j++) {
				for (var k = 0; k < matrix_cols; k++) {
					if (typeof matrix[j][k] !== "number") {
						return false;
					} else {
						continue;
					}
				}

			}
			return true;
		}
	}
}


function validate_matrices(matrix1, matrix2) {
	if (validate_matrix(matrix1) && validate_matrix(matrix2)) {
		var matrix1_cols = matrix1[0].length;
		var matrix2_rows = matrix2.length;
		if (matrix1_cols != matrix2_rows) {
			return false;
		} else {
			return true;
		}
	} else {
		return false;
	}
}
function multiply_2d_matrices(matrix1, matrix2) {
	if (validate_matrices(matrix1, matrix2)) {
		var result = [];
		var matrix1_row_length = matrix1.length;
		var matrix1_col_length = matrix1[0].length;
		var matrix2_col_length = matrix2[0].length;

		for (var row = 0; row < matrix1_row_length; row++) {
			result[row] = []
			for (var col = 0; col < matrix2_col_length; col++) {
				sum = 0;
				for (var k = 0; k < matrix1_col_length; k++) {
					sum += (matrix1[col][k]) * (matrix2[k][col]);
				}
				result[row][col] = sum;
			}
		}
		return result;
	}
	else {
		return "not valid";
	}


}
function multiply_3d_matrices(mat1, mat2) {
	var result3d = [];

	for (var i = 0; i < mat1.length; i++) {
		var ans = multiply_2d_matrices(mat1[i], mat2[i]);

		result3d.push(ans);
	}

	return result3d;
}



var matrix1 = [
	[
		[1, 2, 7],
		[4, 8, 6],
		[1, 2, 4],

	],

	[
		[1, 6, 7],
		[4, 5, 7],
		[2, 3, 4]
	],

	[
		[7, 2, 5],
		[4, 1, 7],
		[1, 4, 3]
	]
];


var matrix2 = [
	[
		[6, 3, 7],
		[4, 6, 9],
		[1, 2, 3]
	],

	[
		[2, 6, 7],
		[4, 3, 7],
		[1, 3, 4]
	],

	[
		[7, 2, 5],
		[4, 1, 7],
		[1, 4, 3]
	]
];
var x = multiply_3d_matrices(matrix1, matrix2)
console.log(x)